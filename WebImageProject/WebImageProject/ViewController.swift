//
//  ViewController.swift
//  WebImageProject
//
//  Created by Rashdan Natiq on 27/03/2017.
//  Copyright © 2017 Devclan. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    
    var imageUrl = [String]()
 

    override func viewDidLoad() {
        super.viewDidLoad()
        imageUrl = ["https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTEW4w_M6B9cgCx-QiDd9mlk7Svpn5k9dT68LKkjfar0Bw---yS_Q", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1t2inOb8bwLWxrqVYFZAZMbbIzYXih-uUh0FWRi9pgKY4zFG8", "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcR6oXjGhBvLjuDF0Atqr5cszsKfbLe00wxnty5j7bPwB41E71Je"]
        
        
    }

 
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        let imageView = cell?.viewWithTag(1) as! UIImageView
        
        imageView.sd_setImage(with: URL(string: imageUrl[indexPath.row]))
        return cell!
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return imageUrl.count
    }
    

}

